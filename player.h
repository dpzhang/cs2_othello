#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Side side;
    Board *board;
    Player(Side side);
    ~Player();
    Side getOtherSide(Side side);
    Move *miniMax(Board *board, Side side, int depth, bool isMax);
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
