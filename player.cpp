#include "player.h"

/**
 * THIS IS A SMALL CHANGE TO MAKE BEFORE SUNDAY MARCH 9 9:00 PDT
 */

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side1) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = true;
    
    board = new Board();
    side = side1;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

/*
 * Gets the color of the other side of the player
 */
Side Player::getOtherSide(Side side)
{
    if (side == BLACK)
    {
        return WHITE;
    }
    else
    {
        return BLACK;
    }
}

/*
 * A recursive function for the minimax algorithm up to a certain given depth
 */
 Move *Player::miniMax(Board *board1, Side side1, int depth, bool isMax)
{
    if (board->hasMoves(side))
    {
        // Base Case
        if (depth == 0)
        {
            int a,b;
            int minScore = 100000;
            Move *nextMove = new Move(0,0);
            Move *minMove = new Move(0,0);
            for (a = 0; a < 8; a++)
            {
                for (b = 0; b < 8; b++)
                {
                    nextMove->setX(a);
                    nextMove->setY(b);
                    if (board1->checkMove(nextMove, side1))
                    {
                        Board *newBoard = board1->copy();
                        newBoard->doMove(nextMove, side1);
                        if ((nextMove->getMoveScore() * newBoard->getScore(side1)) < minScore)
                        {
                            minMove->setX(a);
                            minMove->setY(b);
                        }
                    }
                }
            }
            return minMove;
        }
        if (isMax)
        {
            int maxVal = -100000;
            Move *nextMove1 = new Move(0,0);
            Move *maxMove1 = new Move(0,0);
            Move *maxMove = new Move(0,0);
            int c,d;
            for (c = 0; c < 8; c++)
            {
                for (d = 0; d < 8; d++)
                {
                    nextMove1->setX(c);
                    nextMove1->setY(d);
                    if (board1->checkMove(nextMove1, side1))
                    {
                        Board *newBoard1 = board1->copy();
                        newBoard1->doMove(nextMove1, side1);
                        maxMove1 = miniMax(newBoard1, getOtherSide(side1), depth - 1, false);
                        newBoard1->doMove(maxMove1, getOtherSide(side1));
                        if(maxMove1->getMoveScore() * newBoard1->getScore(side1) > maxVal)
                        {
                            maxVal = maxMove1->getMoveScore() * newBoard1->getScore(side1);
                            maxMove->setX(c);
                            maxMove->setY(d);
                        }
                    }
                }
            }
            return maxMove;
        }
        else
        {
            int minVal = 100000;
            Move *nextMove1 = new Move(0,0);
            Move *minMove1 = new Move(0,0);
            Move *minMove = new Move(0,0);
            int c,d;
            for (c = 0; c < 8; c++)
            {
                for (d = 0; d < 8; d++)
                {
                    nextMove1->setX(c);
                    nextMove1->setY(d);
                    if (board1->checkMove(nextMove1, side1))
                    {
                        Board *newBoard1 = board1->copy();
                        newBoard1->doMove(nextMove1, side1);
                        minMove1 = miniMax(newBoard1, getOtherSide(side1), depth - 1, true);
                        newBoard1->doMove(minMove1, getOtherSide(side1));
                        if(minMove1->getMoveScore() * newBoard1->getScore(side1) < minVal)
                        {
                            minVal = minMove1->getMoveScore() * newBoard1->getScore(side1);
                            minMove->setX(c);
                            minMove->setY(d);
                        }
                    }
                }
            }
            return minMove;
        }
    }
    return NULL;
}
    
/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
     
    // AI that works that can beat the SimplePlayer
    // Update based on opponents move
    board->doMove(opponentsMove, getOtherSide(side));
    Move *nextMove = miniMax(board, side, 4, true);
    if (nextMove == NULL)
    {
        return nextMove;    
    }
    board->doMove(nextMove, side);
    return nextMove;
}
