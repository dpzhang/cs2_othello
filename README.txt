Daniel Zhang

For this week, I built off last week's minimax algorithm to go more than depth 2. In
fact, I used a recursive call on the minimax algorithm so that the algorithm can run
through a given depth. However, this is not very efficient as I have not coded for
iterative deepening. Hence, with a large depth, a result might not be reached in time
for the tournament. However, this code does beat SimplePlayer and beats ConstantTimePlayer
with enough depth. This requires computation time, though. Another aspect that I 
implemented is the weights to certain tiles on the board. More specifically, I added 
multipliers to the corners and the edges of the board. This is because these tiles are
more valuable to be placed on than normal tiles. Hence, it is more beneficial for the AI
to acknowledge the value of these tiles. I tried placing weights on "bad" tiles, but
it somehow did not correctly implement the AI correctly, as it caused my AI to be worse
than it was before as it lost more often. From last week, my implementation of minimax
was not a recursive function, so I had to fully implement it so I could control the depth
of the algorithm.