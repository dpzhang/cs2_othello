#ifndef __COMMON_H__
#define __COMMON_H__

enum Side { 
    WHITE, BLACK
};

class Move{
   
public:
    int x, y;
    Move(int x, int y) {
        this->x = x;
        this->y = y;        
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
    
    int getMoveScore()
    {
        if ((x == 0 && y == 0) || (x == 0 && y == 7)
        || (x == 7 && y == 0) || (x == 7 && y == 7))
        {
            return 5;
        }
        else if((x == 0 && (y == 2 || y == 3 || y == 4 ||
                 y == 5)) || 
               ((x == 2 || x == 3 || x == 4 ||
                 x == 5) && y == 0)||
                (x == 7 && (y == 2 || y == 3 || y == 4 ||
                 y == 5)) ||
                ((x == 2 || x == 3 || x == 4 ||
                 x == 5) && y == 7))
        {
            return 3;
        }
        else
        {
            return 1;
        }
    }
};

#endif
